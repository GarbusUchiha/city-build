import Vue from 'vue'
import Router from 'vue-router'
import firebase from 'firebase'
import store from '../store/index'
import Login from '../views/login.vue'
import register from '../views/register.vue'
import Leaderboard from '../views/leaderboard.vue'
import Account from '../views/account.vue'
import Inhabitiants from '../views/inhabitiants.vue'
import MyCommonalty from '../views/myCommonalty/myCommonalty.vue'
import EventManager from '../views/eventManager/eventManager.vue'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      redirect: 'Login'
    },
    {
      path: '/',
      redirect: 'Login'
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: register
    },
    {
      path: '/leaderboard',
      name: 'leaderboard',
      component: Leaderboard,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/account',
      name: 'account',
      component: Account,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/inhabitiants',
      name: 'inhabitiants',
      component: Inhabitiants,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/my-commonalty',
      name: 'my-commonalty',
      component: MyCommonalty,
      meta: {
        requiresAuth: true
      }
    },
    {
      path: '/event-manager',
      name: 'event-manager',
      component: EventManager,
      meta: {
        requiresAuth: true,
        requiresAdmin: true
      }
    }
  ]
})
router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  let requiresAdmin = to.matched.some(record => record.meta.requiresAdmin)
  if (requiresAuth && !currentUser) { next('login') }
  else if (!requiresAuth && currentUser) { next('leaderboard') }
  else if (requiresAuth && currentUser) {
    let usr = store.getters['user/getUserUid']
    if (usr == null) {
      var user = {
        user: currentUser
      }
      store.dispatch('user/setUser', user)
      let db = firebase.firestore()
      db.settings({ timestampsInSnapshots: true })
      db.collection('accounts').doc(store.getters['user/getUserUid']).get()
        .then(doc => {
          if (!doc.exists) {
            console.log('no document!')
            this.$router.push('/register')
          }
          else {
            store.dispatch('account/setAccount', doc.data())
            usr = store.getters['user/getUserUid']
            if (requiresAdmin) {
              if (requiresAdmin && usr.admin) { next() }
              else { next('leaderboard') }
            }
          }
        })
        .catch(err => {
          console.log('error getting document', err)
        })
    }
    next()
  }
  else {
    next()
  }
})

export default router
