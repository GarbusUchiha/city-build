// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import firebase from 'firebase/app'
import vuex from 'vuex'

Vue.use(firebase)
Vue.use(Vuetify)
Vue.use(vuex)

Vue.config.productionTip = false

let app
let config = {
  apiKey: 'AIzaSyBHCGcG_sFVudN_Cv_ffdqpZ2JSVPh6z8s',
  authDomain: 'city-build-c96ea.firebaseapp.com',
  databaseURL: 'https://city-build-c96ea.firebaseio.com',
  projectId: 'city-build-c96ea',
  storageBucket: 'city-build-c96ea.appspot.com',
  messagingSenderId: '415726510156'
}

firebase.initializeApp(config)
firebase.auth().onAuthStateChanged((user) => {
  if (!app) {
    /* eslint-enable no-new */
    app = new Vue({
      el: '#app',
      router,
      components: { App },
      template: '<App/>'
    })
  }
})
