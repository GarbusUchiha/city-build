import firebase from 'firebase'

export class Event {
  name
  description
  diligence
  character
  accept
  decline

  constructor (data) {
    this.id = data.id
    this.name = data.name
    this.description = data.description
    this.diligence = data.diligence
    this.character = data.character
    this.accept = data.accept
    this.decline = data.decline
  }
}

export class EventCreator {
  name = ''
  description = ''
  diligence = [150, 300]
  character = [150, 300]
  accept = []
  decline = []
  rules = {
    nameRules: [
      v => !!v || 'Name is required',
      v => v.length <= 25 || 'Name must be less than 25 characters',
      v => v.length > 3 || 'Name must be greater than 3 characters'
    ],
    descriptionRules: [
      v => v.length <= 255 || 'Name must be less than 10 characters'
    ]
  };
  effects= [ 'diligence', 'character', 'balance', 'persons' ]

  addNewReaction (type) {
    let newReaction = {
      type: '',
      value: 0,
      self: false,
      random: false
    }
    if (type === 'accept') { this.accept.push(newReaction) }
    else { this.decline.push(newReaction) }
  }

  isValid () {
    if (this.name != '' && this.name.length > 3 && this.description != '')
    { return true }
    else { return false }
  }

  async saveToDb (newEvent) {
    return new Promise(async resolve => {
      let db = firebase.firestore()
      db.settings({timestampsInSnapshots: true})
      db.collection('events').doc(newEvent.id).set(newEvent).then(
        event => {
          resolve()
        },
        err => {
          console.log(err)
          resolve()
        }
      )
    })
  }
}

export default class Occurents {
  Occurents
  empty
  loading
  constructor () {
    this.Occurents = []
    this.loading = true
    this.fetchEvents()
  }

  async fetchEvents () {
    return new Promise(async resolve => {
      let db = firebase.firestore()
      db.settings({timestampsInSnapshots: true})
      db.collection('events').get()
        .then(
          snapshot => {
            if (snapshot.empty) {
              this.empty = true
            }
            else {
              this.empty = false
              snapshot.forEach(doc => {
                let event = new Event(doc.data())
                this.Occurents.push(event)
              })
            }
            this.loading = false
        })
        .catch(err => {
          console.log('Event error')
          console.log(err)
          this.loading = false
        })
        resolve()
    })
  }

  addNewOccurent (data) {
    let event = new Event(data)
    this.Occurents.push(event)
  }

  async removeFromDb (id) {
    return new Promise(async resolve => {
      let db = firebase.firestore()
      db.settings({timestampsInSnapshots: true})
      db.collection('events').doc(id.toString()).delete().then(
        rem => {
          resolve()
        },
        err => {
          console.log(err)
          resolve()
        }
      )
    })
  }
}
