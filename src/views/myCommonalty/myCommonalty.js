import firebase from 'firebase'
import store from '../../store'
import { createId } from '../../components/functions/shared'

export default class Commonalty {
  id
  owner_id
  active
  create_date
  balance
  people
  day

  readCommonalty (doc) {
    this.id = doc.id
    this.owner_id = doc.owner_id
    this.active = doc.active
    this.create_date = doc.create_date
    this.balance = doc.balance
    this.people = doc.people
    this.day = doc.day
  }

  getCommonalty () {
    return {
      id: this.id,
      owner_id: this.owner_id,
      active: this.active,
      create_date: this.create_date,
      balance: this.balance,
      people: this.people,
      day: this.day
    }
  }

  async createCommonalty () {
    return new Promise(async resolve => {
      this.id = createId('com_')
      this.owner_id = store.getters['user/getUserUid']
      this.active = true
      this.create_date = new Date()
      this.balance = 1000
      this.day = 0
      this.people = []
      this.generateRandomPerson()

      let db = firebase.firestore()
      db.settings({timestampsInSnapshots: true})
      db.collection('accounts')
        .doc(store.getters['user/getUserUid'])
        .collection('commonalties')
        .doc('active')
        .set(this.getCommonalty())
        .then(data => {
          store.dispatch('commonalty/setCommonalty', this.getCommonalty())
          resolve()
        },
        err => {
          console.log('waaat?')
          console.log(err)
          resolve()
        }
      )
    })
  }

  unActive () {
    this.active = false
    let newData = {
      id: this.id,
      owner_id: this.owner_id,
      create_date: this.create_date,
      balance: this.balance,
      day: this.day,
      people: this.people
    }

    let db = firebase.firestore()
    db.collection('accounts')
    .doc(store.getters['user/getUserUid'])
    .collection('history')
    .doc(newData.id)
    .set(newData)
    .then(data => {
      console.log('upload git!')
    },
    err => {
      console.log(err)
    })

    db.collection('accounts')
    .doc(store.getters['user/getUserUid'])
    .collection('commonalties')
    .doc('active')
    .delete()
    .then(data => {
    },
    err => {
      console.log(err)
    })
  }

  generateRandomPerson () {
    this.people = [{
      id: createId('p_'),
      number: this.people.length + 1,
      color: this.generateColor(),
      day: 0,
      diligence: 200,
      character: 200,
      work_days: 0,
      profit: 0,
      loss: 0,
      event: {
        history: [],
        current: {},
        done: true
      }
    }]
  };

  addNewPerson (person) {
    var newPerson = {
      id: createId('p_'),
      number: this.people.length + 1,
      color: this.generateColor(),
      day: 0,
      diligence: person.diligence,
      character: person.character,
      work_days: 0,
      profit: 0,
      loss: 0,
      event: {
        history: [],
        current: {},
        done: true
      }
    }
    this.people.push(newPerson)
  }

  addNewRandomPerson () {
    var newPerson = {
      id: createId('p_'),
      number: this.people.length + 1,
      color: this.generateColor(),
      day: 0,
      diligence: Math.floor(Math.random() * 300) + 100,
      character: Math.floor(Math.random() * 300) + 100,
      work_days: 0,
      profit: 0,
      loss: 0,
      event: {
        history: [],
        current: {},
        done: true
      }
    }
    this.people.push(newPerson)
  }
  removePerson (index) {
    this.people[index].disactive = true
  }
  generateColor () {
    var colors = ['red', 'pink', 'purple', 'deep-purple', 'indigo', 'cyan', 'teal', 'light-green', 'lime', 'yellow', 'amber', 'deep-orange', 'brown']
    var rnd = Math.floor(Math.random() * colors.length)
    return colors[rnd]
  }
}
