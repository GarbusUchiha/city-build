import Vue from 'vue'
import Vuex from 'vuex'
import { user } from './user'
import { account } from './account'
import { commonalty } from './commonalty'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    user,
    account,
    commonalty
  }
})
