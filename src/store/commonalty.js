const state = {
  commonalty: {}
}
const mutations = {
  setCommonalty: (state, commonalty) => {
    try {
      state.commonalty = commonalty
    }
    catch (err) {
      console.log(err)
    }
  }
}
const getters = {
  getCommonalty: (state) => {
    try {
      return state.commonalty
    }
    catch (err) {
      console.log(err)
      return ''
    }
  }
}
const actions = {
  setCommonalty: (context, data) => {
    context.commit('setCommonalty', data)
  }
}
export const commonalty = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
