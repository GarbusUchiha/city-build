const state = {
  user: {}
}
const mutations = {
  setUser: (state, user) => {
    try {
      state.user = user.user
    }
    catch (err) {
      console.log(err)
    }
  }
}
const getters = {
  getUser: (state) => {
    try {
      return state.user
    }
    catch (err) {
      console.log(err)
      return ''
    }
  },
  getUserEmail: (state) => {
    try {
      return state.user.email
    }
    catch (err) {
      console.log(err)
      return ''
    }
  },
  getUserUid: (state) => {
    try {
      return state.user.uid
    }
    catch (err) {
      console.log(err)
      return ''
    }
  }
}
const actions = {
  setUser: (context, data) => {
    context.commit('setUser', data)
  }
}
export const user = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
