import firebase from 'firebase'
const state = {
  account: {
    id: '',
    email: '',
    display: '',
    avatar: ''
  }
}
const mutations = {
  setAccount: (state, account) => {
    state.account = account
  },
  setAccountDisplay: (state, display) => {
    return new Promise((resolve) => {
      let db = firebase.firestore()
      db.collection('accounts')
        .doc(state.account.id).get()
        .then((doc) => {
          if (!doc.exists) {
            console.log('Document not exist!')
            state.account = null
            resolve()
          }
          else {
            let account = doc.data()
            account.display = display
            db.collection('accounts')
              .doc(state.account.id).set(account)
              .then(
                () => {
                  state.account.display = display
                  resolve()
                }
              )
              .catch(
                (err) => {
                  console.log(err)
                  resolve()
                }
              )
            }
          })
          .catch(err => {
            console.log(err)
            resolve()
          })
    })
  },
  setAccountAvatar: (state, avatar) => {
    return new Promise((resolve) => {
      let db = firebase.firestore()
      db.collection('accounts')
        .doc(state.account.id).get()
        .then((doc) => {
          if (!doc.exists) {
            console.log('Document not exist!')
            state.account = null
            resolve()
          }
          else {
            let account = doc.data()
            account.avatar = avatar
            db.collection('accounts')
              .doc(state.account.id).set(account)
              .then(
                () => {
                  state.account.avatar = avatar
                  resolve()
                }
              )
              .catch(
                (err) => {
                  console.log(err)
                  resolve()
                }
              )
            }
          })
          .catch(err => {
            console.log(err)
            resolve()
          })
    })
  },

  setAccountByUid: (state, id) => {
    let db = firebase.firestore()
    db.collection('accounts').doc(id).get()
      .then((doc) => {
        if (!doc.exists) {
          state.account = null
        }
        else {
          state.account = doc.data()
        }
      }
    )
      .catch((err) => {
        console.log('Error getting document', err)
      })
  },

  clearData: (state) => {
    state.account = null
  }
}
const getters = {
  getAccount: (state) => {
    return state.account
  },
  getAccountId: (state) => {
    return state.account.id || null
  },
  getAccountDisplay: (state) => {
    return state.account.display
  },
  getAvatar: (state) => {
    return state.account.avatar
  }
}
const actions = {
  setAccountDisplay: async (context, data) => {
    return new Promise(async resolve => {
      await context.commit('setAccountDisplay', data)
      resolve()
    })
  },
  setAccount: (context, data) => {
    context.commit('setAccount', data)
  },
  setAccountAvatar: (context, data) => {
    return new Promise(async resolve => {
      await context.commit('setAccountAvatar', data)
      resolve()
    })
  },
  setAccountByUid: (context, id) => {
    context.commit('setAccountByUid', id)
  },
  logOut: (context) => {
    context.commit('clearData')
  }
}
export const account = {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
