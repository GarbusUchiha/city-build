import firebase from 'firebase'
import { resolve } from 'path';

export let createId = (pre) => {
  let rnd = () => (Math.random() * 46656) | 0
  let str = rnd => ('000' + rnd.toString(36)).slice(-3)
  return `${pre}${Date.now()}${str(rnd())}${str(rnd())}${str(rnd())}`
}
