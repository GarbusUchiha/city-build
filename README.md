# city-build

> A Vue.js / node.js / firebase school project

## How install

``` bash
# instalacja zasobów
npm install

# uruchomienie serwera (hot reload) na localhost:80
npm run dev

```
Piotr Garbicz 2018

Działa na: https://city-build-c96ea.firebaseapp.com/ !


> Szkolny projekt na przedmiot 'Pracownia witryn i aplikacji internetowych' w klasie czwartej,
> semestrze pierwszym w Zespole Szkół Komunikacji w Poznaniu.
> Wykładowca: Prof. Mirosław Szyper.
> Autor projektu: Piotr Garbicz, 4B.

# Założenia projektu
Projekt ma być aplikacją internetową dostępną dla wszystkich użytkowników, w której to wcielamy się w właściciela miasta,
zasilanej ludzką siłą roboczą. Ci ludzie wykonują pracę, oraz różne zdarzenia (każdy człowiek jedno zdarzenie na jeden wirtualny dzień
gry). Zdarzenia mają być różne i z różnych dziedzin, a my możemy takiego człowieka ocenic za czyn dokonany -nagrodzić lub ukarać.
Na bazie naszych decyzji będzie on wyciągał wnioski i uczył się, które zachowania są poprawne, a które nie. Z czasem
przybywać będą (lub odchodzić czy umierać) nowi członkowie, którzy też będą naszymi podmiotami do rozważań.

Będziemy mieli okazję wcielić się w 'wychowywawce sztucznej inteligencji'. Celem aplikacji jest pokazanie, że nie ważne, co zrobimy,
'ludzki' (tym razem symulowany) czynnik zawsze będzie prowadził do destrukcji.

A przy okazji powstaną jakieś połączenia z bazą danych, template w html, może 'kilka' skryptów.


# Opis pracy nad projektem:
Jako że użyłem w swoim projekcie niestandardowych technologii (w szkole, lecz nie na rynku pracy), postanowiłem go wstępnie trochę omówić.
Projekt został napisany pod serwer oparty na node.js i webpacku z myślą o 'Single-page application' oraz frameworku vue.js.
Baza danych znajduje sięna silniku firebase od firmy google (baza obiektowa JSON). Do front-endu użyłem elementów UI material design również od firmy google, w bibliotece vuetify (material design pod komponenty vue.js).

Aby uruchomić ten projekt lokalnie potrzebny jest serwer node.js, uruchamiany przez cli, a do niego npm (node package manager).
Oczywiście byłoby to głupotą, gdyby była to jedyna weryfikacja tego kodu, dlatego jest on uruchomiony i w pełni działający na hostingu
pod adresem: https://city-build-c96ea.firebaseapp.com/ (https :O :O :O).

Zapraszam do rejestracji :)

Poza oczywistościami (jak zapamiętywanie sesji, zadbanie o pełne SPA, pełną responsywność czy minifikację i aglyfikację kodu na serwerze)
zadbałem o przejrzystość strony. Jak wiadomo, jest to pierwszy etap kontrolny oddania projektu. Tak jak kod źródłowy mogę bez problemu podać
(w razie czego łatwo go znaleźć tu - https://gitlab.com/GarbusUchiha/city-build), tak baza danych w formacie JSON jest średnio użyteczna, a jej wykonanie średnio mierzalne (jednak jest w pełni wykonana i działająca - zapraszam do testowania logowania :) )

# Wykonane etapy:
 - Implementacja projektu
 - Połączenie z bazą, projekt bazy, obsługa logowania i rejestracji
 - stworzenie widoków, obsłużenie przekierowań
 - projekt oraz wdrożenie funkcjonalności i logiki aplikacji

# etapy do wykonania:
 - obsługa pomocy użytkowników (przywracanie hasła)
 - testy
 - development aplikacji

